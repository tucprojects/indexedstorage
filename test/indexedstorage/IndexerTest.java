/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

import org.junit.Test;

import static org.junit.Assert.*;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.IOException;

/**
 *
 * @author archi
 */
public class IndexerTest {

    // File car_all_com has exactly 1400 documents.
    // If tests isn't passed:
    // a) The num of docs in the file has changed or
    // b) Something wrong is done in the index function
    @Test
    public void indexRAMTest() throws IOException {

        // Test using **RAMDirectory**
        String  cran     = "cran_dataset/cra_all_com";
        Indexer mIndexer = new Indexer(new RAMDirectory(),
                                       new StandardAnalyzer(Version.LUCENE_29),
                                       cran, null);
        int     result   = mIndexer.index();

        mIndexer.close();
        assertTrue(result == 1400);
    }

    @Test
    public void indexFSTest() throws IOException {

        // Test using **FSDirectory**
        String  cran     = "cran_dataset/cra_all_com";
        File    tmp_dir  = new File(System.getProperty("java.io.tmpdir"));
        Indexer mIndexer = new Indexer(FSDirectory.open(tmp_dir),
                                       new StandardAnalyzer(Version.LUCENE_29),
                                       cran, null);
        int     result   = mIndexer.index();

        mIndexer.close();
        assertTrue(result == 1400);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
