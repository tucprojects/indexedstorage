/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;

//~--- JDK imports ------------------------------------------------------------

import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author archi
 */
public abstract class AbstractSearchStrategy implements SearchStrategy {
    abstract public Query constructQuery(String query_str);

    public void searchAndWrite(String queryStr, int qryNum, Searcher s,
                               FileOutputStream fstream) {
        String hits = null;

        hits = search(queryStr, qryNum, s);
        resultsToFile(hits, fstream);
    }

    public void resultsToFile(String hits, FileOutputStream fstream) {
        byte data[] = hits.getBytes();

        try {
            fstream.write(data, 0, data.length);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public String search(String queryStr, int qryNum, Searcher s) {
        Query      query = constructQuery(queryStr);
        ScoreDoc[] hits  = null;

        try {
            hits = s.search(query);
        } catch (IOException ex) {}

        StringBuilder results = new StringBuilder();

        for (int i = 0; i < hits.length; ++i) {
            Document hitDoc = null;

            try {
                hitDoc = s.getSearcher().doc(hits[i].doc);
            } catch (CorruptIndexException ex) {}
            catch (IOException ex) {}

            results.append(qryNum);
            results.append("\t");
            results.append(hitDoc.get("Number"));
            results.append("\n");
        }

        return results.toString();
    }
}


