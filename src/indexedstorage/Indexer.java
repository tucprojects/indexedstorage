/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.search.DefaultSimilarity;
import org.apache.lucene.search.Similarity;
import org.apache.lucene.store.Directory;

//~--- JDK imports ------------------------------------------------------------

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

import java.util.Scanner;

/**
 *
 * @author archi
 */
public final class Indexer implements Closeable {

    /**
     * TODO: Pass a Similarity object to change the scoring
     * in function index *before* indexing.
     * (IndexWriter.setSimilarity())
     */
    private final StandardAnalyzer analyzer_;
    private final String           filepath_;
    private final Directory        index_;
    private final Similarity       similarity_;

    public Indexer(Directory dir, StandardAnalyzer analyzer, String Filename,
                   Similarity s) {
        index_    = dir;
        analyzer_ = analyzer;
        filepath_ = Filename;

        if (s == null) {
            similarity_ = new DefaultSimilarity();
        } else {
            similarity_ = s;
        }
    }

    public Directory getIndex() {
        return index_;
    }

    public void close() throws IOException {
        index_.close();
    }

    public int index() throws IOException {
        IndexWriter writer =
            new IndexWriter(index_, analyzer_, true,
                            IndexWriter.MaxFieldLength.UNLIMITED);

        writer.setSimilarity(similarity_);

        Scanner scanner = new Scanner(new File(filepath_));

        scanner.useDelimiter("\\n\\n");

        int           count      = 1;
        StringBuilder strBuilder = new StringBuilder();

        while (scanner.hasNext()) {
            strBuilder.setLength(0);
            strBuilder.append(scanner.next());

            StringBuilder docNum = new StringBuilder();
            int           end    = 0;

            for (int i = 0; i < strBuilder.length(); ++i) {
                if (Character.isDigit(strBuilder.charAt(i))) {
                    docNum.append(strBuilder.charAt(i));
                } else if (strBuilder.charAt(i) == '\n') {
                    end = ++i;  // remove number and \n

                    break;
                }
            }

            strBuilder.delete(0, end);
            addDocument(writer, docNum.toString(), strBuilder.toString());
            ++count;
        }

        writer.close();
        scanner.close();

        return count;
    }

    private void addDocument(IndexWriter w, String docNum, String sValue)
            throws IOException {
        Document doc = new Document();

        doc.add(new Field("Number", docNum, Field.Store.YES,
                          Field.Index.ANALYZED));
        doc.add(new Field("Doc", sValue, Field.Store.YES,
                          Field.Index.ANALYZED));
        w.addDocument(doc);
    }
}


