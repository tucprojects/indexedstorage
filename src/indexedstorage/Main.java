/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.lucene.analysis.standard.StandardAnalyzer;

//import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

//~--- JDK imports ------------------------------------------------------------

//import org.apache.lucene.store.RAMDirectory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author archi
 */
public class Main {

    /**
     * The project code fully supports lucene version 2.9.2. The problem with
     * 3.0.2 is the implementation of OKAPI (class Scorer has changed).
     * TODO: Port OKAPI to lucene 3.0.2.
     *
     * @param args
     * @throws IOException
     */
    public static void main(String args[]) throws IOException {
        if (args.length == 0) {
            System.out.println("Usage: java -jar indexedstorage.jar "
                    + "cran_all_com cran_qry_com");
            System.exit(1);
        }

        String  bqRes    = "bq_results";
        String  vsmRes   = "vsm_results";
        String  bm25Res  = "bm25_results";
        
        String  cranData = args[0]; //"cran_dataset/cran_all_com";
        String  cranQry  = args[1]; //"cran_dataset/cran_qry_com";

//      File    tmpDir   = new File(System.getProperty("java.io.tmpdir"));
//      Indexer mIndexer = new Indexer(FSDirectory.open(tmpDir),
//                                     new StandardAnalyzer(Version.LUCENE_29),
//                                     cranData, null);
        Indexer mIndexer = new Indexer(new RAMDirectory(),
                                       new StandardAnalyzer(Version.LUCENE_29),
                                       cranData, null);

        mIndexer.index();

        FileOutputStream  bqStream   = new FileOutputStream(new File(bqRes));
        FileOutputStream  vsmStream  = new FileOutputStream(new File(vsmRes));
        FileOutputStream  bm25Stream = new FileOutputStream(new File(bm25Res));
        Searcher          search     = new Searcher(mIndexer.getIndex(), null);
        ArrayList<String> queries    = new ArrayList<String>();
        StringBuilder     qryBuilder = new StringBuilder();
        Scanner           scanner    = new Scanner(new File(cranQry));
        String            pattern    = "\\n\\n";

        scanner.useDelimiter(pattern);

        while (scanner.hasNext()) {
            qryBuilder.setLength(0);
            qryBuilder.append(scanner.next());
            queries.add(qryBuilder.toString());
        }

        AbstractSearchStrategy vsmStrategy  = new VsmStrategy();
        AbstractSearchStrategy boolStrategy = new BooleanStrategy();
        AbstractSearchStrategy bm25Strategy = new Bm25Strategy();

        for (int num = 0; num < queries.size(); ++num) {
            String qryStr = queries.get(num);
            int    qryNum = num + 1;  // Utils.extractQueryNumber(qryStr);

            boolStrategy.searchAndWrite(qryStr, qryNum, search, bqStream);
            vsmStrategy.searchAndWrite(qryStr, qryNum, search, vsmStream);
            bm25Strategy.searchAndWrite(qryStr, qryNum, search, bm25Stream);
        }

        mIndexer.close();
        bqStream.close();
        vsmStream.close();
        bm25Stream.close();
    }
}


