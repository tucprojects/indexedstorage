/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;

import org.ninit.models.bm25.BM25BooleanQuery;
import org.ninit.models.bm25f.BM25FParameters;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

/**
 *
 * @author archi
 */
public class Bm25Strategy extends AbstractSearchStrategy {
    public Bm25Strategy() {}

    public Query constructQuery(String query_str) {
        String[] fields = { "Number", "Doc" };

        BM25FParameters.setAverageLength("Number", 3.2f);
        BM25FParameters.setAverageLength("Doc", 1000.0f);

        BM25BooleanQuery queryF = null;

        try {
            queryF =
                new BM25BooleanQuery(query_str, fields,
                                     new StandardAnalyzer(Version.LUCENE_29));
        } catch (ParseException ex) {}
        catch (IOException ex) {}

        return queryF;
    }
}


