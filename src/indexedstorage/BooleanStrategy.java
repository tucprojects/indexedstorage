/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.Version;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;
import java.io.StringReader;

/**
 *
 * @author archi
 */
public class BooleanStrategy extends AbstractSearchStrategy {
    public BooleanStrategy() {}

    public Query constructQuery(String query_str) {
        StandardAnalyzer analyzer     = new StandardAnalyzer(Version.LUCENE_29);
        BooleanQuery     booleanQuery = new BooleanQuery();
        TokenStream      tokStr       = null;

        try {

            // deprecated in 2.9.2 but not in 3.0.2
            tokStr = analyzer.reusableTokenStream("Doc",
                                                  new StringReader(query_str));

            // version 3.0.2+
            TermAttribute termAtt =
                (TermAttribute) tokStr.addAttribute(TermAttribute.class);

            // TermAttribute termAtt = null;
            tokStr.reset();

            while (tokStr.incrementToken()) {
                Query query = new TermQuery(new Term("Doc", termAtt.term()));

                booleanQuery.add(query, BooleanClause.Occur.SHOULD);
            }

            tokStr.end();
            tokStr.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return booleanQuery;
    }
}


