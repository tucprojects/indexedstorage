/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

/**
 *
 * @author archi
 */
public class Utils {

    /**
     * extractQueryNumber takes a query as a String and extracts the number
     * in front of it.
     * @param queryStr
     *                  The query as a string
     * @return
     */
    public static int extractQueryNumber(String queryStr) {
        StringBuilder numQueryStr = new StringBuilder();
        char          c;

        for (int i = 0; i < queryStr.length(); ++i) {
            c = queryStr.charAt(i);

            if (c == '\n') {
                continue;
            }

            if (Character.isDigit(c)) {
                numQueryStr.append(c);

                continue;
            }

            break;
        }

        int numQueryInt = Integer.parseInt(numQueryStr.toString());

        return numQueryInt;
    }
}


