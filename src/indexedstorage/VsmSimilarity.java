/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.search.Similarity;

/**
 *
 * @author archi
 */
public class VsmSimilarity extends Similarity {

    /**
     * The following code implements the Vector Space Model.
     * This class is the same (almost) as DefaultSimilarity.
     * This class exists only for testing things and nothing
     * more.
     */
    static final long serialVersionUID = -5456437233070650890L;
    protected boolean discountOverlaps = false;

    public void setDiscountOverlaps(boolean v) {
        discountOverlaps = v;
    }

    public boolean getDiscountOverlaps() {
        return discountOverlaps;
    }

    @Override
    public float computeNorm(String field, FieldInvertState state) {
        final int numTerms;

        if (discountOverlaps) {
            numTerms = state.getLength() - state.getNumOverlap();
        } else {
            numTerms = state.getLength();
        }

        return (state.getBoost() * lengthNorm(field, numTerms));
    }

    @Override
    public float lengthNorm(String fieldName, int numTokens) {
        return (float) (1.0 / Math.sqrt(numTokens));
    }

    @Override
    public float queryNorm(float sumOfSquaredWeights) {
        return (float) (1.0 / Math.sqrt(sumOfSquaredWeights));
    }

    @Override
    public float sloppyFreq(int distance) {
        return (float) 1.0 / (distance + 1);
    }

    @Override
    public float tf(float freq) {
        return (float) Math.sqrt(freq);
    }

    @Override
    public float idf(int docFreq, int numDocs) {
        return (float) (1 + Math.log10(numDocs / (docFreq + 1)));
    }

    @Override
    public float coord(int overlap, int maxOverlap) {
        return overlap / (float) maxOverlap;
    }
}


