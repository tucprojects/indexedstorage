/**
 * This file is part of IndexedStorage
 *
 * IndexedStorage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010 Vourlakis Nikolas
 */



package indexedstorage;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.lucene.search.DefaultSimilarity;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Similarity;
import org.apache.lucene.store.Directory;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

/**
 *
 * @author archi
 */
public final class Searcher {
    private Directory        filepath_;
    private IndexSearcher    iSearcher_;
    private final Similarity similarity_;

    public Searcher(Directory filepath, Similarity similarity) {
        filepath_  = filepath;
        iSearcher_ = null;

        if (similarity == null) {
            similarity_ = new DefaultSimilarity();
        } else {
            similarity_ = similarity;
        }
    }

    public IndexSearcher getSearcher() throws IOException {
        lazySearcher();

        return iSearcher_;
    }

    public ScoreDoc[] search(Query q) throws IOException {
        lazySearcher();

        ScoreDoc[] hits = iSearcher_.search(q, 20).scoreDocs;

        return hits;
    }

    private void lazySearcher() throws IOException {
        if (iSearcher_ == null) {
            iSearcher_ = new IndexSearcher(filepath_, true);
            iSearcher_.setSimilarity(similarity_);
        }
    }
}


