clc;
clear all;
close all;

recall = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

bm25 = [ 80.60, 75.19, 71.29, 61.08, 60.37, 59.73, 58.91, 60.68, ...
         58.67, 56.01, 54.54 ];
bool = [ 81.66, 78.05, 75.01, 63.07, 65.16, 63.40, 55.87, 57.5, ...
         54.46, 65.33, 64.80 ];
vsm  = [ 81.29, 77.40, 74.95, 65.65, 66.81, 64.77, 57.98, 59.98, ...
         57.03, 62.27, 61.17  ];

figure;
plot(recall, bool, ...
     recall, vsm, ...
     recall, bm25);
 grid on;
 axis([0 100 0 100]);
 xlabel('Recall');
 ylabel('Presicion');
 title('Precision-Recall diagram for the 3 models');
 legend('Boolean Model', 'Vector Space Model', 'OKAPI - BM25');